package skio

import (
	"errors"
	"fmt"
	"g06-cart-service/common"

	"github.com/gin-gonic/gin"
	socketio "github.com/googollee/go-socket.io"
	"github.com/googollee/go-socket.io/engineio"
	"github.com/googollee/go-socket.io/engineio/transport"
	"github.com/googollee/go-socket.io/engineio/transport/websocket"
	"gorm.io/gorm"
	"log"
	"sync"
)

type RealtimeEngine interface {
	UserSockets(userId int) []AppSocket
	EmitToRoom(room string, key string, data interface{}) error
	EmitToUser(userId int, key string, data interface{}) error
	GetUser(conn socketio.Conn) common.Requester
}

type AppContext interface {
	GetMainDBConnection() *gorm.DB
	SecretKey() string
	GetRealtimeEngine() RealtimeEngine
	GetUser(conn socketio.Conn) common.Requester
}

type rtEngine struct {
	server      *socketio.Server
	storage     map[int][]AppSocket
	storageConn map[string]AppSocket
	locker      *sync.RWMutex
}

func NewRtEngine() *rtEngine {
	return &rtEngine{
		storage:     make(map[int][]AppSocket),
		storageConn: make(map[string]AppSocket),
		locker:      new(sync.RWMutex),
	}
}

func (engine *rtEngine) saveAppSocket(userId int, appSk AppSocket) {
	engine.locker.Lock()
	defer engine.locker.Unlock()
	if v, ok := engine.storage[userId]; ok {
		engine.storage[userId] = append(v, appSk)
	} else {
		engine.storage[userId] = []AppSocket{appSk}
	}

	engine.storageConn[appSk.ID()] = appSk
}

func (engine *rtEngine) getAppSocket(userId int) []AppSocket {
	engine.locker.RLock()
	defer engine.locker.RUnlock()

	return engine.storage[userId]
}

func (engine *rtEngine) removeAppSocket(userId int, appSck AppSocket) {
	engine.locker.Lock()
	defer engine.locker.Unlock()
	if v, ok := engine.storage[userId]; ok {
		for i := range v {
			if v[i] == appSck {
				engine.storage[userId] = append(v[:i], v[i+1:]...)
				break
			}
		}
	}
	delete(engine.storageConn, appSck.ID())
}

func (engine *rtEngine) GetUser(conn socketio.Conn) common.Requester {
	ask, _ := engine.GetAppSocket(conn)
	return ask
}
func (engine *rtEngine) GetAppSocket(conn socketio.Conn) (AppSocket, error) {
	if appSocket, ok := engine.storageConn[conn.ID()]; ok {
		return appSocket, nil
	}

	return nil, errors.New("socket not found")
}

func (engine *rtEngine) UserSockets(userId int) []AppSocket {
	var socket []AppSocket
	engine.locker.RLock()
	defer engine.locker.Unlock()
	if scks, ok := engine.storage[userId]; ok {
		return scks
	}

	return socket
}

func (engine *rtEngine) EmitToRoom(room string, key string, data interface{}) error {
	engine.server.BroadcastToRoom("/", room, key, data)
	return nil
}

func (engine *rtEngine) EmitToUser(userId int, key string, data interface{}) error {
	sockets := engine.storage[userId]

	for _, s := range sockets {
		s.Emit(key, data)
	}

	return nil
}

func (engine *rtEngine) Run(appCtx AppContext, r *gin.Engine) error {
	server := socketio.NewServer(&engineio.Options{
		Transports: []transport.Transport{websocket.Default},
	})
	engine.server = server

	server.OnConnect("/", func(s socketio.Conn) error {
		fmt.Println("Socket connected:", s.ID(), " IP:", s.RemoteAddr())
		s.Join("Shipper")

		return nil
	})

	//server.OnEvent("/", "authenticate", func(s socketio.Conn, token string) {
	//	db := appCtx.GetMainDBConnection()
	//	store := userstore.NewSQLStore(db)
	//
	//	tokenProvider := jwt.NewTokenJWTProvider(appCtx.SecretKey())
	//
	//	payload, err := tokenProvider.Validate(token)
	//	if err != nil {
	//		s.Emit("authentication_failed", err.Error())
	//		s.Close()
	//		return
	//	}
	//
	//	user, err := store.FindUser(context.Background(), map[string]interface{}{"id": payload.UserId})
	//
	//	if err != nil {
	//		s.Emit("authentication_failed", err.Error())
	//		s.Close()
	//		return
	//	}
	//
	//	if user.Status == 0 {
	//		s.Emit("authentication_failed", err.Error())
	//		s.Close()
	//		return
	//	}
	//	user.Mask(false)
	//
	//	appSck := NewAppSocket(s, user)
	//	engine.saveAppSocket(user.ID, appSck)
	//	s.Emit("authenticated", user)
	//
	//	return
	//})

	server.OnDisconnect("/", func(s socketio.Conn, msg string) {
		fmt.Println("Socket disconnected:", s.ID(), " IP:", s.RemoteAddr())
		appSck, err := engine.GetAppSocket(s)
		if err != nil {
			log.Println("error::", err)
		}
		engine.removeAppSocket(appSck.GetUserId(), appSck)
	})

	//server.OnEvent("/", "test", func(s socketio.Conn, msg string) {
	//	log.Println("test:", msg)
	//})
	//
	//server.OnEvent("/", string(common.TopicUserUpdateLocation), func(s socketio.Conn, msg interface{}) {
	//	fmt.Println("Socket disconnected:", s.ID(), " IP:", s.RemoteAddr())
	//	fmt.Println("msg:", msg)
	//})

	go server.Serve()

	r.GET("/socket.io/*any", gin.WrapH(server))
	r.POST("/socket.io/*any", gin.WrapH(server))
	return nil
}
