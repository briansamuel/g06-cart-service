package httpClient

import (
	"bytes"
	"encoding/json"
	"g06-cart-service/common"
	log "github.com/sirupsen/logrus"
	"io/ioutil"
	"net/http"
	"time"
)

type httpClient struct {
	client *http.Client
}

func NewHttpClient() *httpClient {
	var myClient = &http.Client{Timeout: 30 * time.Second}
	return &httpClient{client: myClient}
}

func (c *httpClient) getJson(req *http.Request, target interface{}) error {

	r, err := c.client.Do(req)
	if err != nil {
		return err
	}
	defer r.Body.Close()

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Println("Error while reading the response bytes:", err)
	}
	log.Println(string([]byte(body)))

	err = json.Unmarshal(body, target)
	if err != nil {
		return err
	}
	return nil

}

func (c *httpClient) GetJson(req *http.Request, target interface{}) error {

	err := c.getJson(req, target)
	if err != nil {
		return common.ErrEntityCannotParseJson("Interface", err)
	}
	return nil
}

func (c *httpClient) sendRequest(method, url, token string, body, target interface{}) error {

	if method == "" {
		method = "GET"
	}

	var bearer = "Bearer " + token
	out, err := json.Marshal(body)
	if err != nil {
		log.Fatal(err)
	}
	req, err := http.NewRequest(method, url, bytes.NewBuffer(out))
	if err != nil {
		log.Info(err)
	}

	// add authorization header to the req
	req.Header.Add("Authorization", bearer)

	if err := c.getJson(req, target); err != nil {
		log.Info(err)
		return err
	}
	return nil
}

func (c *httpClient) SendGet(url, token string, target interface{}) error {

	err := c.sendRequest("GET", url, token, nil, target)
	if err != nil {
		return common.ErrInvalidRequest(err)
	}
	return nil
}

func (c *httpClient) SendPost(url, token string, body, target interface{}) error {

	err := c.sendRequest("POST", url, token, body, target)
	if err != nil {
		return common.ErrInvalidRequest(err)
	}
	return nil
}
