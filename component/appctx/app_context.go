package appctx

import (
	"g06-cart-service/common"
	"g06-cart-service/kafka"
	"g06-cart-service/skio"
	socketio "github.com/googollee/go-socket.io"
	"gorm.io/gorm"
)

type AppContext interface {
	GetMainDBConnection() *gorm.DB
	SecretKey() string
	GetKafka() kafka.PubSub
	GetRealtimeEngine() skio.RealtimeEngine
	GetUser(conn socketio.Conn) common.Requester
	ListService() common.ListService
	GetService(key string) string
}

type appCtx struct {
	db *gorm.DB

	secretKey   string
	rtEngine    skio.RealtimeEngine
	client      kafka.PubSub
	listService common.ListService
}

func NewAppContext(db *gorm.DB, secretKey string, rtEngine skio.RealtimeEngine, client kafka.PubSub, listService common.ListService) *appCtx {
	return &appCtx{db: db, secretKey: secretKey, rtEngine: rtEngine, client: client, listService: listService}
}

func (ctx *appCtx) GetMainDBConnection() *gorm.DB { return ctx.db }

func (ctx *appCtx) SecretKey() string { return ctx.secretKey }

func (ctx *appCtx) GetKafka() kafka.PubSub { return ctx.client }

func (ctx *appCtx) GetRealtimeEngine() skio.RealtimeEngine { return ctx.rtEngine }

func (ctx *appCtx) GetUser(conn socketio.Conn) common.Requester {
	return ctx.GetRealtimeEngine().GetUser(conn)
}

func (ctx *appCtx) ListService() common.ListService { return ctx.listService }

func (ctx *appCtx) GetService(key string) string { return ctx.listService.GetService(key) }
