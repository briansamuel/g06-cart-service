package common

type SimpleFood struct {
	SQLModel    `json:",inline"`
	Name        string  `json:"name" gorm:"column:name;"`
	Description string  `json:"description" gorm:"column:description;"`
	Price       float64 `json:"price" gorm:"column:price"`
	Images      *Images `json:"avatar" gorm:"avatar"`
}

func (SimpleFood) TableName() string { return "foods" }

func (data *SimpleFood) Mask(isOwnerOrAdmin bool) {
	data.GenUID(DbTypeFood)
}
