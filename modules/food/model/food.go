package foodmodel

import "g06-cart-service/common"

// +gen slice:"Where,Count,GroupBy[string]"
type Food struct {
	ID          int            `json:"-" gorm:"id"`
	FakeID      common.UID     `json:"id" gorm:"-"`
	Name        string         `json:"name" gorm:"column:name;"`
	Description string         `json:"description" gorm:"column:description;"`
	Price       float32        `json:"price" gorm:"column:price"`
	Images      *common.Images `json:"avatar" gorm:"avatar"`
}

func (u *Food) Unmask() {
	uid, _ := common.FromBase58(u.FakeID.String())
	u.ID = int(uid.GetLocalID())

}

func (u *Food) Mask(isOwnerOrAdmin bool) {
	u.GenUID(common.DbTypeFood)

}

func (u *Food) GenUID(objType int) {
	u.FakeID = common.NewUID(uint32(u.ID), objType, 1)
}
