package grpcclientcart

import (
	"context"
	"g06-cart-service/common"
	foodmodel "g06-cart-service/modules/food/model"
	food_service "g06-cart-service/proto/food"
)

type grpcClient struct {
	client food_service.FoodServiceClient
}

func NewGRPCClient(client food_service.FoodServiceClient) *grpcClient {
	return &grpcClient{client: client}
}

func (c *grpcClient) ListFoodInCart(ctx context.Context, ids []int, token string) ([]*foodmodel.Food, error) {
	newIds := make([]int32, len(ids))

	for i := range newIds {
		newIds[i] = int32(ids[i])
	}

	res, err := c.client.GetListFood(ctx, &food_service.ListFoodRequest{ResIds: newIds})

	if err != nil {
		return nil, common.ErrDB(err)
	}
	var foods []*foodmodel.Food
	for _, v := range res.Result {
		foods = append(foods, &foodmodel.Food{ID: int(v.Id), Name: v.Name, Price: v.Price})
	}

	return foods, nil
}
