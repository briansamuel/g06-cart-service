package cartstorage

import (
	"context"
	"g06-cart-service/common"
	cartmodel "g06-cart-service/modules/cart/model"
)

func (s *sqlStore) ListDataWithCondition(ctx context.Context,
	filter *cartmodel.Filter,
	paging *common.Paging) ([]cartmodel.Cart, error) {
	db := s.db
	var result []cartmodel.Cart

	db = db.Where("status in (?)", 1)

	if filter.UserId > 0 {
		db = db.Where("user_id = ?", filter.UserId)
	}

	if filter.FoodId > 0 {
		db = db.Where("food_id = ?", filter.FoodId)
	}

	if err := db.Table(cartmodel.Cart{}.TableName()).Count(&paging.Total).Error; err != nil {
		return nil, common.ErrDB(err)
	}

	if err := db.
		Limit(paging.Limit).
		Offset((paging.Page - 1) * paging.Limit).
		Order("created_at desc").
		Find(&result).Error; err != nil {

		return nil, common.ErrDB(err)
	}

	return result, nil
}
