package cartstorage

import (
	"context"
	"g06-cart-service/common"
	cartmodel "g06-cart-service/modules/cart/model"
)

func (s *sqlStore) Delete(ctx context.Context, filter *cartmodel.Filter) error {
	db := s.db

	db = db.Where("status in (?)", 1)

	if filter.UserId > 0 {
		db = db.Where("user_id = ?", filter.UserId)
	}

	if filter.FoodId > 0 {
		db = db.Where("food_id = ?", filter.FoodId)
	}

	if err := db.Table(cartmodel.Cart{}.TableName()).Delete(nil).Error; err != nil {

		return common.ErrDB(err)
	}

	return nil
}
