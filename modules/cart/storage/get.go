package cartstorage

import (
	"context"
	"g06-cart-service/common"
	cartmodel "g06-cart-service/modules/cart/model"
	"gorm.io/gorm"
)

func (s *sqlStore) GetDataWithCondition(ctx context.Context,
	cond map[string]interface{}) (*cartmodel.Cart, error) {

	var data cartmodel.Cart
	if err := s.db.
		Where(cond).
		First(&data).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, common.RecordNotFound
		}
		return nil, common.ErrDB(err)
	}

	return &data, nil
}
