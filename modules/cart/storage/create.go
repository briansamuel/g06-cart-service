package cartstorage

import (
	"context"
	"g06-cart-service/common"
	cartmodel "g06-cart-service/modules/cart/model"
)

func (s *sqlStore) Create(ctx context.Context, data *cartmodel.CartCreate) error {
	db := s.db

	if err := db.Create(data).Error; err != nil {
		return common.ErrDB(err)
	}

	return nil
}
