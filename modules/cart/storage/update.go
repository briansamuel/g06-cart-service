package cartstorage

import (
	"context"
	"g06-cart-service/common"
	cartmodel "g06-cart-service/modules/cart/model"
)

func (s *sqlStore) Update(ctx context.Context, id int, data *cartmodel.CartUpdate) error {
	db := s.db

	if err := db.Where("id = ?", id).Updates(&data).Error; err != nil {
		return common.ErrDB(err)
	}

	return nil
}
