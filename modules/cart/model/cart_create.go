package cartmodel

import (
	"time"
)

type CartCreate struct {
	UserId    int        `json:"user_id" gorm:"column:user_id"`
	FoodId    int        `json:"food_id" gorm:"column:food_id"`
	Quantity  int        `json:"quantity" gorm:"column:quantity;"`
	CreatedAt *time.Time `json:"createdAt" gorm:"column:created_at;"` // Thêm * vào type để lấy null
	Status    int        `json:"status" gorm:"column:status;default:1;"`
}

func (CartCreate) TableName() string { return Cart{}.TableName() }
