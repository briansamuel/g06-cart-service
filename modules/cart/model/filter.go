package cartmodel

type Filter struct {
	UserId int `json:"user_id" gorm:"column:user_id"`
	FoodId int `json:"food_id" gorm:"column:food_id"`
}

func (Filter) TableName() string { return Cart{}.TableName() }
