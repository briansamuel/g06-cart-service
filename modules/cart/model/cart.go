package cartmodel

import (
	foodmodel "g06-cart-service/modules/food/model"
	"time"
)

const EntityName = "Cart"

type Cart struct {
	FoodId    int             `json:"-" gorm:"food_id"`
	Food      *foodmodel.Food `json:"food" gorm:"-"`
	Quantity  int             `json:"quantity" gorm:"column:quantity;"`
	CreatedAt *time.Time      `json:"createdAt" gorm:"column:created_at;"` // Thêm * vào type để lấy null
	UpdatedAt *time.Time      `json:"updatedAt" gorm:"column:updated_at;"` // Thêm * vào type để lấy null
	Status    int             `json:"status" gorm:"column:status;default:1;"`
}

func (Cart) TableName() string { return "carts" }
