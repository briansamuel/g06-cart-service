package cartmodel

type CartUpdate struct {
	Quantity int `json:"quantity" gorm:"column:quantity;"`
}

func (CartUpdate) TableName() string { return Cart{}.TableName() }
