package cartbusiness

import (
	"context"
	"g06-cart-service/common"
	cartmodel "g06-cart-service/modules/cart/model"
)

type GetCartStorage interface {
	GetDataWithCondition(ctx context.Context,
		cond map[string]interface{}) (*cartmodel.Cart, error)
}

type getCartBiz struct {
	store GetCartStorage
}

func NewGetCartBiz(store GetCartStorage) *getCartBiz {
	return &getCartBiz{store: store}
}

func (biz *getCartBiz) GetCart(ctx context.Context, id int) (*cartmodel.Cart, error) {
	data, err := biz.store.GetDataWithCondition(ctx, map[string]interface{}{"id": id})
	if err != nil {
		if err == common.RecordNotFound {
			return nil, common.ErrEntityNotFound(cartmodel.EntityName, err)
		}
		return nil, common.ErrCannotGetEntity(cartmodel.EntityName, err)
	}
	if data.Status == 0 {
		return nil, common.ErrEntityDeleted(cartmodel.EntityName, err)
	}
	return data, nil
}
