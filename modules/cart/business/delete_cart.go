package cartbusiness

import (
	"context"
	"g06-cart-service/common"
	cartmodel "g06-cart-service/modules/cart/model"
)

type DeleteCartStorage interface {
	Delete(ctx context.Context, filter *cartmodel.Filter) error
	GetDataWithCondition(ctx context.Context,
		cond map[string]interface{}) (*cartmodel.Cart, error)
}

type deleteCartBiz struct {
	store DeleteCartStorage
}

func NewDeleteCartBiz(store DeleteCartStorage) *deleteCartBiz {
	return &deleteCartBiz{store: store}
}

func (biz *deleteCartBiz) DeleteCart(ctx context.Context, filter *cartmodel.Filter) error {
	oldData, err := biz.store.GetDataWithCondition(ctx, map[string]interface{}{"user_id": filter.UserId})

	if err != nil {
		return common.ErrCannotUpdateEntity(cartmodel.EntityName, err)
	}
	if oldData.Status == 0 {
		return common.ErrEntityNotFound(cartmodel.EntityName, err)
	}

	if err := biz.store.Delete(ctx, filter); err != nil {
		return common.ErrCannotCreateEntity(cartmodel.EntityName, err)
	}

	return nil
}
