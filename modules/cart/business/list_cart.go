package cartbusiness

import (
	"context"
	"g06-cart-service/common"
	cartmodel "g06-cart-service/modules/cart/model"
	foodmodel "g06-cart-service/modules/food/model"
)

type ListCartStorage interface {
	ListDataWithCondition(ctx context.Context,
		filter *cartmodel.Filter,
		paging *common.Paging) ([]cartmodel.Cart, error)
}

type FoodStorage interface {
	ListFoodInCart(ctx context.Context, ids []int, token string) ([]*foodmodel.Food, error)
}

type listCartBiz struct {
	store       ListCartStorage
	foodStorage FoodStorage
}

func NewListCartBiz(store ListCartStorage, foodStorage FoodStorage) *listCartBiz {
	return &listCartBiz{store: store, foodStorage: foodStorage}
}

func (biz *listCartBiz) ListCart(ctx context.Context,
	filter *cartmodel.Filter,
	paging *common.Paging, token string) ([]cartmodel.Cart, error) {
	result, err := biz.store.ListDataWithCondition(ctx, filter, paging)
	if err != nil {
		return nil, common.ErrCannotListEntity(cartmodel.EntityName, err)
	}
	var foodIDArray []int
	for _, item := range result {
		foodIDArray = append(foodIDArray, item.FoodId)
	}
	if len(foodIDArray) > 0 {
		foods, err := biz.foodStorage.ListFoodInCart(ctx, foodIDArray, token)
		if err == nil {
			for i, _ := range result {

				for j, food := range foods {
					food.Mask(false)
					if result[i].FoodId == food.ID {
						result[i].Food = foods[j]
					}
				}
			}
		}
	}
	return result, nil
}
