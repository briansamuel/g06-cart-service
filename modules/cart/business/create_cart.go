package cartbusiness

import (
	"context"
	"g06-cart-service/common"
	cartmodel "g06-cart-service/modules/cart/model"
)

type CreateCartStorage interface {
	Create(ctx context.Context, data *cartmodel.CartCreate) error
}

type createCartBiz struct {
	store CreateCartStorage
}

func NewCreateCartBiz(store CreateCartStorage) *createCartBiz {
	return &createCartBiz{store: store}
}

func (biz *createCartBiz) CreateCart(ctx context.Context, data *cartmodel.CartCreate) error {
	err := biz.store.Create(ctx, data)
	if err != nil {
		return common.ErrCannotCreateEntity(cartmodel.EntityName, err)
	}

	return nil
}
