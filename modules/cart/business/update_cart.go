package cartbusiness

import (
	"context"
	"g06-cart-service/common"
	cartmodel "g06-cart-service/modules/cart/model"
)

type UpdateCartStorage interface {
	Update(ctx context.Context, id int, data *cartmodel.CartUpdate) error
	GetDataWithCondition(ctx context.Context,
		cond map[string]interface{}) (*cartmodel.Cart, error)
}

type updateCartBiz struct {
	store UpdateCartStorage
}

func NewUpdateCartBiz(store UpdateCartStorage) *updateCartBiz {
	return &updateCartBiz{store: store}
}

func (biz *updateCartBiz) UpdateCart(ctx context.Context, id, userId int, data *cartmodel.CartUpdate) error {
	oldData, err := biz.store.GetDataWithCondition(ctx, map[string]interface{}{"id": id, "user_id": userId})

	if err != nil {
		return common.ErrCannotUpdateEntity(cartmodel.EntityName, err)
	}
	if oldData.Status == 0 {
		return common.ErrEntityNotFound(cartmodel.EntityName, err)
	}

	if err := biz.store.Update(ctx, id, data); err != nil {

		return common.ErrCannotUpdateEntity(cartmodel.EntityName, err)
	}
	return nil
}
