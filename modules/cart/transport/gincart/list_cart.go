package gincart

import (
	"g06-cart-service/common"
	"g06-cart-service/component/appctx"
	cartbusiness "g06-cart-service/modules/cart/business"
	cartmodel "g06-cart-service/modules/cart/model"
	cartstorage "g06-cart-service/modules/cart/storage"
	grpcclientcart "g06-cart-service/modules/cart/storage/grpcclient"
	food_service "g06-cart-service/proto/food"
	"github.com/gin-gonic/gin"
	"google.golang.org/grpc"
	"net/http"
)

func ListCart(appContext appctx.AppContext, grpcClientConn *grpc.ClientConn) func(ctx *gin.Context) {
	return func(c *gin.Context) {
		var filter cartmodel.Filter
		var paging common.Paging
		paging.Process()
		if err := c.ShouldBind(&filter); err != nil {
			panic(common.ErrInternal(err))
		}

		requester := c.MustGet(common.CurrentUser).(common.Requester)
		filter.UserId = requester.GetUserId()

		store := cartstorage.NewSQLStore(appContext.GetMainDBConnection())
		foodStore := grpcclientcart.NewGRPCClient(food_service.NewFoodServiceClient(grpcClientConn))
		biz := cartbusiness.NewListCartBiz(store, foodStore)

		result, err := biz.ListCart(c.Request.Context(), &filter, &paging, requester.GetToken())
		if err != nil {
			panic(err)
		}
		data := map[string]interface{}{"cart": result, "user": requester}
		c.JSON(http.StatusOK, common.NewSuccessResponse(data, paging, filter))
	}
}
