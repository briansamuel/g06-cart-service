package gincart

import (
	"g06-cart-service/common"
	"g06-cart-service/component/appctx"
	cartbusiness "g06-cart-service/modules/cart/business"
	cartmodel "g06-cart-service/modules/cart/model"
	cartstorage "g06-cart-service/modules/cart/storage"
	"github.com/gin-gonic/gin"
	"net/http"
)

func CreateCart(appContext appctx.AppContext) func(ctx *gin.Context) {
	return func(c *gin.Context) {
		var newCart cartmodel.CartCreate

		if err := c.ShouldBind(&newCart); err != nil {
			panic(common.ErrInternal(err))
		}

		requester := c.MustGet(common.CurrentUser).(common.Requester)
		newCart.UserId = requester.GetUserId()
		store := cartstorage.NewSQLStore(appContext.GetMainDBConnection())
		biz := cartbusiness.NewCreateCartBiz(store)
		if err := biz.CreateCart(c.Request.Context(), &newCart); err != nil {
			panic(err)
		}

		c.JSON(http.StatusOK, common.SimpleSuccessResponse(cartmodel.EntityName+"Successes"))
	}
}
