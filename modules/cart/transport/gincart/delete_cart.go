package gincart

import (
	"g06-cart-service/common"
	"g06-cart-service/component/appctx"
	cartbusiness "g06-cart-service/modules/cart/business"
	cartmodel "g06-cart-service/modules/cart/model"
	cartstorage "g06-cart-service/modules/cart/storage"
	"github.com/gin-gonic/gin"
	"net/http"
)

func DeleteItemCart(appContext appctx.AppContext) func(ctx *gin.Context) {
	return func(c *gin.Context) {
		var filter cartmodel.Filter
		foodId, err := common.FromBase58(c.Param("food_id"))

		if err != nil {
			panic(common.ErrInternal(err))
		}

		if err != nil {
			panic(common.ErrInternal(err))
		}

		requester := c.MustGet(common.CurrentUser).(common.Requester)
		filter.UserId = requester.GetUserId()
		filter.FoodId = int(foodId.GetLocalID())
		store := cartstorage.NewSQLStore(appContext.GetMainDBConnection())
		biz := cartbusiness.NewDeleteCartBiz(store)

		if err := biz.DeleteCart(c.Request.Context(), &filter); err != nil {
			panic(err)
		}
		c.JSON(http.StatusOK, common.SimpleSuccessResponse(filter))
	}
}

func EmptyCart(appContext appctx.AppContext) func(ctx *gin.Context) {
	return func(c *gin.Context) {
		var filter cartmodel.Filter
		requester := c.MustGet(common.CurrentUser).(common.Requester)
		filter.UserId = requester.GetUserId()
		store := cartstorage.NewSQLStore(appContext.GetMainDBConnection())
		biz := cartbusiness.NewDeleteCartBiz(store)

		if err := biz.DeleteCart(c.Request.Context(), &filter); err != nil {
			panic(err)
		}
		c.JSON(http.StatusOK, common.SimpleSuccessResponse(filter))
	}
}
