package main

import (
	"fmt"
	"g06-cart-service/common"
	"g06-cart-service/component/appctx"
	"g06-cart-service/kafka"
	"g06-cart-service/kafka/kafkapb"
	"g06-cart-service/middleware"
	"g06-cart-service/modules/cart/transport/gincart"
	"g06-cart-service/skio"
	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
	"github.com/mattn/go-colorable"
	log "github.com/sirupsen/logrus"
	"google.golang.org/grpc"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"os"
	"strings"
	"time"
)

func main() {
	formatter := new(log.TextFormatter)
	formatter.ForceColors = true
	formatter.TimestampFormat = "2006-01-02 15:04:05"
	formatter.FullTimestamp = true
	log.SetFormatter(formatter)
	log.SetOutput(colorable.NewColorableStdout())
	// Config Log
	if os.Getenv("GIN_MODE") == "RELEASE" {
		log.SetLevel(log.InfoLevel)
	} else {
		log.SetLevel(log.TraceLevel)
	}

	log.Info("Setup Config Log")
	// End Config Log

	err := godotenv.Load()
	if err != nil {
		log.Info("Error loading .env file")
	}

	dbHost := os.Getenv("DB_HOST")
	dbDatabase := os.Getenv("DB_NAME")
	dbPort := os.Getenv("DB_PORT")
	dbUser := os.Getenv("DB_USER")
	dbPassword := os.Getenv("DB_PASSWORD")
	secretKey := os.Getenv("SYSTEM_SECRET")
	kafkaBroker := os.Getenv("KAFKA_BROKER")
	ginPort := os.Getenv("GIN_PORT")
	gRPCFood := os.Getenv("GRPC_FOOD")
	if ginPort == "" {
		ginPort = ":8080"
	}

	if err != nil {
		log.Info("Error loading .env file")
	}

	opts := grpc.WithInsecure()

	cc, err := grpc.Dial(gRPCFood, opts)
	if err != nil {
		log.Fatal(err)
	}

	defer cc.Close()
	mysqlConnection := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8mb4&parseTime=true&loc=Local", dbUser, dbPassword, dbHost, dbPort, dbDatabase)

	db, err := gorm.Open(mysql.Open(mysqlConnection), &gorm.Config{})
	log.Info("Connect Mysql")
	if err != nil {
		log.Fatalln("Error mysql:", err)
	}
	db = db.Debug()

	sqlDB, err := db.DB()

	// SetMaxIdleConns sets the maximum number of connections in the idle connection pool.
	sqlDB.SetMaxIdleConns(10)

	// SetMaxOpenConns sets the maximum number of open connections to the database.
	sqlDB.SetMaxOpenConns(200)

	// SetConnMaxLifetime sets the maximum amount of time a connection may be reused.
	sqlDB.SetConnMaxLifetime(time.Hour)

	log.Info("Setup Gin Router")
	r := gin.Default()

	rtEngine := skio.NewRtEngine()
	brokers := strings.Split(kafkaBroker, ",")

	client := kafkapb.NewKafkaPubSub(brokers...)

	service := map[string]string{
		"AUTH_SERVICE":       os.Getenv("AUTH_SERVICE"),
		"RESTAURANT_SERVICE": os.Getenv("RESTAURANT_SERVICE"),
		"UPLOAD_SERVICE":     os.Getenv("UPLOAD_SERVICE"),
	}
	listService := *common.NewService(service)
	appCtx := appctx.NewAppContext(db, secretKey, rtEngine, client, listService)
	_ = rtEngine.Run(appCtx, r)
	_ = kafka.NewSubscriber(appCtx).Start()

	r.Use(middleware.Recover(appCtx))

	middlewareAuth := middleware.RequireAuth(appCtx)
	v1 := r.Group("/v1")
	{

		// Orders
		cart := v1.Group("/cart", middlewareAuth)
		{
			cart.POST("", gincart.CreateCart(appCtx))

			cart.GET("", gincart.ListCart(appCtx, cc))

			cart.DELETE("/:id", gincart.EmptyCart(appCtx))

		}

	}
	// Test CI CD
	if err := r.Run(ginPort); err != nil {
		return
	} // listen and serve on 0.0.0.0:8080 (for windows "localhost:8080")
}
